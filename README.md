# IcelineTheme
Custom theme made for the iceline host.

## Installation:
```sh
bash <(curl https://gitlab.com/josiahrs/icelinetheme/-/raw/main/scripts/install.sh)
```
