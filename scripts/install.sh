#!/bin/bash

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

clear

installTheme(){
    cd /var/www/
    tar -cvf IcelineThemeBackup.tar.gz pterodactyl
    echo "Installing theme..."
    cd /var/www/pterodactyl
    rm -r IcelineTheme
    git clone https://gitlab.com/josiahrs/icelinetheme
    cd IcelineTheme
    rm /var/www/pterodactyl/resources/scripts/IcelineTheme.css
    rm /var/www/pterodactyl/resources/scripts/index.tsx
    mv index.tsx /var/www/pterodactyl/resources/scripts/index.tsx
    mv IcelineTheme.css /var/www/pterodactyl/resources/scripts/IcelineTheme.css
    cd /var/www/pterodactyl

    curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
    apt update
    apt install -y nodejs

    npm i -g yarn
    yarn

    cd /var/www/pterodactyl
    yarn build:production
    sudo php artisan optimize:clear


}

installThemeQuestion(){
    while true; do
        read -p "Are you sure that you want to install the theme [y/n]? " yn
        case $yn in
            [Yy]* ) installTheme; break;;
            [Nn]* ) exit;;
            * ) echo "Please answer [Y]es or [N]o.";;
        esac
    done
}

repair(){
    bash <(curl https://gitlab.com/josiahrs/icelinetheme/main/script/repair.sh)
}

echo "Copyright (c) 2022 Josiah"
echo "This program is free software: you can redistribute it and/or modify"
echo ""
echo "[1] Install theme"
echo "[2] Repair panel"
echo "[3] Exit"

read -p "Please enter a number: " choice
if [ $choice == "1" ]
    then
    installThemeQuestion
fi
if [ $choice == "2" ]
    then
    repair
fi
if [ $choice == "3" ]
    then
    exit
fi
